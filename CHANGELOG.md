# Change Log

- 3.0
  - Licence change (Hippocratic 2.1)
  - Switched to microbundle build system
  - Removed 3 unnecessary exports

- 2.2.0
  - Switched to jest testing.
  - Changed names for async function versions from "...P" to "...Async".
    - But the old ones are still there and works.
  - Dependencies upgraded.
