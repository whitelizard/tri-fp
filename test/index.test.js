import * as S from 'sanctuary';
import { bi, biAsync, tri, triAsync, tryCatch, tryCatchAsync, setNoneValue } from '../src';

const excFunc = () => {
  JSON.parse('&');
};
const errFunc = () => {
  throw Error('message');
};

describe('tri', () => {
  it('should return result in tuple', () => {
    const func = (x) => x * 2;
    const [err, res] = tri(func)(3);
    expect(res).toBe(6);
    expect(err).toBeUndefined();
  });
  it('should not throw custom errors', () => {
    const [err, res] = tri(errFunc)();
    expect(res).toBeUndefined();
    expect(err.message).toBe('message');
    expect(err.constructor).toBe(Error);
  });
  it('should throw native exceptions', () => {
    const func = tri((a) => a.b);
    expect(func).toThrow(TypeError);
  });
});

describe('triAsync', () => {
  it('should return result in tuple', async () => {
    const [err, res] = await triAsync(Promise.resolve(5));
    expect(res).toBe(5);
    expect(err).toBeUndefined();
  });
  it('should accept promise returning function', async () => {
    const getPromise = (x) => Promise.resolve(x);
    const [err, res] = await triAsync(getPromise)(5);
    expect(res).toBe(5);
    expect(err).toBeUndefined();
  });
  it('should not throw custom/application errors', async () => {
    const [err, res] = await triAsync(Promise.reject(Error('message')));
    expect(res).toBeUndefined();
    expect(err.constructor).toBe(Error);
    expect(err.message).toBe('message');
    const [err2, res2] = await triAsync(() => new Promise(errFunc))();
    expect(res2).toBeUndefined();
    expect(err2.constructor).toBe(Error);
    expect(err2.message).toBe('message');
  });
  it('should throw native exceptions (reject promise)', async () => {
    await expect(triAsync(async (a) => a.b)()).rejects.toBeInstanceOf(TypeError);
    await expect(triAsync(async () => JSON.parse('&'))()).rejects.toBeInstanceOf(SyntaxError);
  });
});

describe('bi', () => {
  it('should convert exceptions to application errors', () => {
    const [err, res] = bi(excFunc)();
    expect(err.constructor).toBe(Error);
    expect(err.constructor !== SyntaxError).toBeTruthy();
    expect(res).toBeUndefined();
  });
  it('should not convert application errors', () => {
    const error = Error('message');
    const thrower = () => {
      throw error;
    };
    const [err, res] = bi(thrower)();
    expect(err).toBe(error);
    expect(res).toBeUndefined();
  });
});

describe('biAsync', () => {
  it('should convert exceptions to application errors', async () => {
    const [err, res] = await biAsync(() => new Promise(excFunc))();
    expect(err.constructor).toBe(Error);
    expect(err.constructor !== SyntaxError).toBeTruthy();
    expect(res).toBeUndefined();
  });
});

describe('tryCatch', () => {
  it('should not convert exceptions to application errors', () => {
    const [err, res] = tryCatch(excFunc)();
    expect(err.constructor).toBe(SyntaxError);
    expect(err.constructor !== Error).toBeTruthy();
    expect(res).toBeUndefined();
  });
  it('tryCatch func should not convert application errors', () => {
    const error = Error('message');
    const thrower = () => {
      throw error;
    };
    const [err, res] = tryCatch(thrower)();
    expect(err).toBe(error);
    expect(res).toBeUndefined();
  });
});

describe('tryCatchAsync', () => {
  it('tryCatchAsync func should not throw errors', async () => {
    const [err, res] = await tryCatchAsync(Promise.reject(Error('message')));
    expect(res).toBeUndefined();
    expect(err.constructor).toBe(Error);
    expect(err.message).toBe('message');
    const [err2, res2] = await tryCatchAsync(() => new Promise(excFunc))();
    expect(res2).toBeUndefined();
    expect(err2.constructor).toBe(SyntaxError);
  });
});

describe('custom none value', () => {
  it('should affect all none return values', async () => {
    setNoneValue(null);
    const [, res] = await tryCatchAsync(Promise.reject(Error('message')));
    expect(res).toBeNull();
    const [, res2] = tri(errFunc)();
    expect(res2).toBeNull();
    const [err3] = await triAsync(Promise.resolve(5));
    expect(err3).toBeNull();
    const [, res4] = await triAsync(Promise.reject(Error('message')));
    expect(res4).toBeNull();
    const func = (x) => x * 2;
    const [err5] = tri(func)(3);
    expect(err5).toBeNull();
  });
});

describe('special use cases', () => {
  it('should be possible to use with Either', () => {
    const arrayPairToEither = ([e, r]) => (e ? S.Left(e) : S.Right(r));
    const triE = (f) => S.unchecked.pipe([tri(f), arrayPairToEither]);
    const result = triE(errFunc)();
    expect(S.isLeft(result)).toBeTruthy();
    expect(S.either(S.show)(S.I)(result)).toBe('new Error ("message")');
    // t.equals(err.constructor, Error);
    const func = triE((a) => a.b);
    expect(func).toThrow(TypeError);
  });
});
