// native errors
const exceptions = [EvalError, RangeError, ReferenceError, SyntaxError, TypeError, URIError].filter(
  (ex) => typeof ex === 'function',
);

const conf = {};
export const setNoneValue = (x) => (conf.none = x);

const throwNative = (err) => {
  if (exceptions.includes(err.constructor)) throw err;
  return err;
};

const convertNative = (err) => {
  if (!exceptions.includes(err.constructor)) return err;
  const error = Error(err.message);
  error.stack = err.stack;
  return error;
};

/**
 * @func
 * @param {Error} errTransf Possible error
 * @returns {Function}
 */
export const tryWrap =
  (errTransf) =>
  (func) =>
  (...args) => {
    try {
      const result = func(...args);
      return [conf.none, result];
    } catch (err) {
      return [errTransf ? errTransf(err) : err, conf.none];
    }
  };
export const tri = tryWrap(throwNative);
export const bi = tryWrap(convertNative);
export const tryCatch = tryWrap();

export const tryWrapAsync = (errTransf) => (promise) =>
  typeof promise === 'function'
    ? (...args) => tryWrapAsync(errTransf)(promise(...args))
    : promise.then(
        (data) => [conf.none, data],
        (err) => [errTransf ? errTransf(err) : err, conf.none],
      );
export const tryWrapP = tryWrapAsync;
export const triP = tryWrapAsync(throwNative);
export const triAsync = triP;
export const biP = tryWrapAsync(convertNative);
export const biAsync = biP;
export const tryCatchP = tryWrapAsync();
export const tryCatchAsync = tryCatchP;
